#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import glob

from setuptools import setup, find_packages

def get_version(package):
    """Directly retrieve version, avoiding an import

    Since setup.py runs before the package is set up, we can't expect
    that simply doing an import ._version will work reliably in all
    cases.  Instead, manually import the version from the file here,
    and then the module can be imported elsewhere in the project easily.
    """
    version_file = "%s/%s" %(package, '_version.py')
    version_string = open(version_file, "rt").read()
    re_version = r"^__version__ = ['\"]([^'\"]*)['\"]"
    m = re.search(re_version, version_string, re.M)
    if not m:
        raise RuntimeError("Unable to find version string for %s in %s." %(
            package, version_file))
    return m.group(1)

def get_description():
    return open('README.md', 'rt').read()


setup(
    name             = 'diskwatch',
    version          = '0.1',
    url              = 'none',
    author           = 'Bryce Harrington',
    author_email     = 'bryce@bryceharrington.org',
    description      = 'Displays information about local hard disks',
    long_description = open('README.md', 'rt').read(),
    license          = 'AGPL',
    platforms        = ['any'],
    setup_requires   = [
        'pytest-runner'
        ],
    tests_require    = [
        'pytest',
        'pep8',
        'pyflakes',
        ],
    install_requires = [
        'argparse',
        'ruamel.yaml'
        ],
    packages         = find_packages(),
    package_data     = { },
    data_files       = [ ],
    scripts          = glob.glob('scripts/*'),

)
