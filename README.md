diskwatch displays information about system's hard disks in convenient
and digestable forms.  


## Usage Examples

  TBD


## Prerequisites:

python3-ruamel.yaml


## Testing

$ python3 setup.py check


## Installation

$ sudo python3 setup.py install


## Packaging

$ python3 setup.py sdist
